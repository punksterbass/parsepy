# -*- coding: utf-8 -*-
import music21 as m21

# função que retorna zero caso a nota seja uma pausa,
# a lista de notas individuais caso seja um acorde, e o valor midi caso seja uma nota com altura
def midi_note(nota):
  if isinstance(nota, m21.note.Rest):
    return 0
  elif isinstance(nota, m21.chord.Chord):
    return [x.pitch.midi for x in nota]
  else:
    return nota.pitch.midi


# retorna 1 se for uma nota ou pausa, e len(x) caso x seja um acorde
def multiplicity(nota):
  if isinstance(nota, m21.chord.Chord):
    return len(nota)
  else:
    return 1


# extrai as partes do arquivo musicxml
def extract_parts(filename):
    piece = m21.converter.parse(filename).voicesToParts().stripTies()
    raw_parts = [x for x in piece.getElementsByClass("Part")]
    parts = []
    for part in raw_parts:
        new_part = part.flat.getElementsByClass("GeneralNote")
        filled_part = new_part.makeRests(fillGaps=True, inPlace=False)
        parts.append(filled_part)
    return parts

# função que pega um stream individual, como uma Parte ou uma Voz
# e faz o parse dele, criando um dicionário do tipo {offset: [duração, valor midi, multiplicidade]}
def parse_stream(stream):
  offset_dict = dict()
  for nota in stream.flat.getElementsByClass("GeneralNote").stripTies():
    offset_dict[nota.offset] = [round(nota.quarterLength, 4), midi_note(nota), multiplicity(nota)]
  return offset_dict


# junta pausas consecutivas em uma entidade só
def combine_rests(part):
    parsed = parse_stream(part)
    keys = sorted(parsed.keys())
    raw_vector = [parsed[key] for key in keys]
    for i in range(len(keys)):
        raw_vector[i].insert(0, keys[i])
    k = len(raw_vector)
    i = 1
    while i < k:
        if raw_vector[i][2] == 0:
            if raw_vector[i-1][2] == 0:
                raw_vector[i-1][1] += raw_vector[i][1]
                del raw_vector[i]
                k -= 1
                i = i
            else:
                i += 1
        else:
            i += 1
    new_dict = dict()
    for vector in raw_vector:
        new_dict[vector[0]] = [vector[1], vector[2], vector[3]]
    return new_dict    

# dado um offset e uma parte já parsed pela função acima,
# me retorna o offset mais próximo do offset dado que pertença a parte dada
def closest_offset(parsed_stream, off):
  return max([x for x in sorted(parsed_stream.keys()) if x <= off])

# retorna todos os offsets de um stream
def all_offsets(parts):
    result = []
    for part in parts:
        result += list(part.keys())
    return sorted(list(set(result)))

# a função checa se, dado um offset e uma parte, a parte está soando (sem ser pausa)
# nesse offset
def isplaying(parsed_part, off):
  closest = closest_offset(parsed_part, off)
  if closest + parsed_part[closest][0] >= off and parsed_part[closest][1] != 0:
    return [closest, parsed_part[closest][0], parsed_part[closest][2]]

# dado um offset e todas as partes, faz a análise particional desse offset
def analysis(offset, parts):
    result = []
    for part in parts:
        isplayingResult = isplaying(part, offset)
        if isplayingResult:
            i = 0
            while i < isplayingResult[2]:
                new_result = isplayingResult[0:2]
                result.append(tuple(new_result))
                i += 1
    return sorted([result.count(x) for x in list(set(result))])

# dada uma lista de elementos, elimina os elementos consecutivos
# dessa maneira, nossa análise particional não terá partições consecutivas iguais
def remove_consecutive(lista):
  i = 0
  while i < len(lista)-1:
    if lista[i] == lista[i+1]:
        del lista[i]
    else:
        i = i+1
  return lista


# dada a lista de partes já parsed e todos os offsets, retorna a análise particional
def final_analysis(parts, offsets, full=False):
  if full:
    return [analysis(off, parts) for off in offsets if analysis(off, parts) != []]
  else:
    return remove_consecutive([analysis(off, parts) for off in offsets if analysis(off, parts) != []])


# faz a análise toda tendo como input o nome do arquivo musicxml
def partitional_analysis(filename, full=False):
    raw_parts = extract_parts(filename)
    parts = [combine_rests(x) for x in raw_parts]
    offsets = all_offsets(parts)
    return final_analysis(parts, offsets, full=full)
